﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitPortal : MonoBehaviour
{
    [SerializeField]
    int levelToLoad;

    [SerializeField]
    bool autoIndex = true;

    private void Start()
    {
        if(autoIndex)
        {
            levelToLoad = SceneManager.GetActiveScene().buildIndex + 1;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            Debug.Log("Level Won !");
            //On save le niveau à charger pour le bouton 'CONTINUE'
            PlayerPrefs.SetInt("LastLevel", levelToLoad);
            //On doit charger le niveau suivant
            SceneManager.LoadScene(levelToLoad);
        }
    }
}
