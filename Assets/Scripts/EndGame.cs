﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    [SerializeField]
    GameObject player;

    [SerializeField]
    GameObject affichageCommande;

    [SerializeField]
    GameObject vueCinematique;

    [SerializeField]
    GameObject spaceship;

    [SerializeField]
    GameObject affichageEscape;

    private Animator spaceshipAnimator;
    private AudioSource spaceshipAudio;
    private Camera cameraCinematique;

    // Start is called before the first frame update
    void Start()
    {
        player.SetActive(true);
        affichageCommande.SetActive(false);

        cameraCinematique = vueCinematique.GetComponent<Camera>();
        vueCinematique.SetActive(false);

        spaceshipAnimator = spaceship.GetComponent<Animator>();
        spaceshipAudio = spaceship.GetComponent<AudioSource>();
        spaceshipAnimator.enabled = false;
        spaceshipAudio.enabled = false;
        affichageEscape.SetActive(false);
    }

    // OnTriggerEnter est appelé quand le Collider other entre dans le déclencheur
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            affichageCommande.SetActive(true);
        }
    }

    // OnTriggerExit est appelé quand le Collider other cesse de toucher le déclencheur
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            affichageCommande.SetActive(false);
        }
    }

    // OnTriggerStay est appelé une fois par trame pour chaque Collider other qui touche le déclencheur
    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Return))
        {
            affichageCommande.SetActive(false);
            player.SetActive(false);
            vueCinematique.SetActive(true);

            spaceshipAudio.enabled = true;
            spaceshipAnimator.enabled = true;
            
            spaceshipAudio.Play();

            StartCoroutine(GoToMenuAfterEnding());
        }
    }

    //Coroutine pour attendre un peu avant d'aller au menu principal
    IEnumerator GoToMenuAfterEnding()
    {
        yield return new WaitForSeconds(3.5f);
        affichageEscape.SetActive(true);
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene("Menu");
    }
}
