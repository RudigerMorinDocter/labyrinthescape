﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HelpControls : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool mouse_over = false;

    [SerializeField]
    GameObject helpText;

    [SerializeField]
    GameObject iconQuestion;

    [SerializeField]
    Camera mainCamera;

    [SerializeField]
    Sprite currentImage, transparentImage;

    Image imageQuestion;

    // Start is called before the first frame update
    void Start()
    {
        iconQuestion.SetActive(true);
        helpText.SetActive(false);
        imageQuestion = iconQuestion.GetComponent<Image>();
    }

    void Update()
    {
        if (mouse_over)
        {
            helpText.SetActive(true);
            imageQuestion.sprite = transparentImage;
        }
        else
        {
            helpText.SetActive(false);
            imageQuestion.sprite = currentImage;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouse_over = true;
        Debug.Log("Mouse enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouse_over = false;
        Debug.Log("Mouse exit");
    }
}
