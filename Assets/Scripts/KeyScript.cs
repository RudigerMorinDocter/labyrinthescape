﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    //OnCollisionEnter quand on collide avec !
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            GameObject.Find("DoorExit").GetComponent<DoorScript>().CanOpen = true;
            GetComponent<AudioSource>().Play(); //Afin d'avoir le temps de jouer le son, on va juste désactiver les visuels, puis effacer dans 3 secondes
            GetComponent<MeshRenderer>().enabled = false;   //On désactive le mesh
            GetComponent<BoxCollider>().enabled = false;    //On désactive le collider
            Destroy(transform.parent.gameObject, 3f);   //On détruit mon parent dans 3secondes
        }
    }
}
