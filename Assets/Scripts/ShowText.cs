﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowText : MonoBehaviour
{
    [SerializeField]
    GameObject textFinal;

    // Start is called before the first frame update
    void Start()
    {
        textFinal.GetComponent<MeshRenderer>().enabled = false;
    }

    // OnTriggerEnter est appelé quand le Collider other entre dans le déclencheur
    private void OnTriggerEnter(Collider other)
    {
        textFinal.GetComponent<MeshRenderer>().enabled = true;
    }

    // OnTriggerExit est appelé quand le Collider other cesse de toucher le déclencheur
    private void OnTriggerExit(Collider other)
    {
        textFinal.GetComponent<MeshRenderer>().enabled = false;
    }


}
