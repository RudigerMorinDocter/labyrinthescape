﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnBall : MonoBehaviour
{
    [SerializeField]
    Transform myTarget;

    [SerializeField]
    MovingBall movingBallScript;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "IntelligentBall")
        {
            movingBallScript.target = myTarget;
        }
    }
}
