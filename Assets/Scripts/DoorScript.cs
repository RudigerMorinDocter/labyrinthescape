﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    //Var pour savoir si on peut ouvrir la porte
    public bool CanOpen = false;

    [SerializeField]
    AudioClip soundOpen, soundDenied;

    [SerializeField]
    Animator doorAnimator;

    [SerializeField]
    GameObject portal;

    private AudioSource myAudioSource;

    //On choppe notre audio source
    private void Awake()
    {
        this.myAudioSource = this.GetComponent<AudioSource>();
    }

    //OnTriggerEnter pour détecter le joueur
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && CanOpen)
        {
            //On joue le bruit d'ouverture et on ouvre la porte
            portal.SetActive(true);
            myAudioSource.PlayOneShot(soundOpen);
            doorAnimator.enabled = true;
            
        }
        else if(other.tag == "Player" && !CanOpen)
        {
            //On joue le bruit de buzz
            myAudioSource.PlayOneShot(soundDenied);
        }
    }
}
