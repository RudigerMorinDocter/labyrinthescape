﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    [SerializeField]
    private GameObject continueButton;

    //Start
    public void Start()
    {
        continueButton.GetComponent<Button>().interactable = false;
    }

    //Update
    public void Update()
    {
        if(PlayerPrefs.GetInt("LastLevel") > 1)
        {
            continueButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            continueButton.GetComponent<Button>().interactable = false;
        }
    }

    //On appuie sur Exit
    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    //On appuie sur Play
    public void PlayGame()
    {
        SceneManager.LoadScene("PreGame");
        //SceneManager.LoadScene(1);    //C'est le même délire
    }

    //On appuie sur Continue
    public void ContinueGame()
    {
        int levelToLoad = PlayerPrefs.GetInt("LastLevel");

        if(levelToLoad > 1)
        {
            SceneManager.LoadScene(levelToLoad);
        }
    }
}
