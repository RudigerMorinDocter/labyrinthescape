﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownScript : MonoBehaviour
{
    [SerializeField]
    private int timeLeft = 60;

    [SerializeField]
    private Text textTempsRestant;

    // Start is called before the first frame update
    void Start()
    {
        textTempsRestant.text = "Time Left : " + timeLeft;
        StartCoroutine(Countdown());
    }

    IEnumerator Countdown()
    {
        while(timeLeft > 0)
        {
            yield return new WaitForSeconds(1f);
            timeLeft--;
            textTempsRestant.text = "Time Left : " + timeLeft;
        }

        //Ici le temps est écoulé
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GameOver();
    }
}
