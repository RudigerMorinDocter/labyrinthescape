﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SkipIntroduction : MonoBehaviour
{
    [SerializeField]
    GameObject affichageCommande;

    bool canClick = false;

    // Start est appelé juste avant qu'une méthode Update soit appelée pour la première fois
    private void Start()
    {
        canClick = false;
        StartCoroutine(WaitingBeforeShowing());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey && canClick)
        {
            SceneManager.LoadScene("Level1");
            Debug.Log("A key or mouse click has been detected");
        }
    }

    //Attendre un peu avant d'indiquer ce qu'il faut faire
    IEnumerator WaitingBeforeShowing()
    {
        yield return new WaitForSeconds(5f);
        canClick = true;
        affichageCommande.GetComponent<Text>().enabled = true;
    }
}
