﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //Variable de vitesse de déplacement
    [SerializeField]
    private float moveSpeed = 3f;
    [SerializeField]
    private float turnSpeed = 100f;
    private float currentSpeed;

    AudioSource playerAudio;

    [SerializeField]
    private GameObject affichageGameOver;

    private void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Savoir si on cours
        if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            currentSpeed = 2 * moveSpeed;
        }
        else
        {
            currentSpeed = moveSpeed;
        }

        //Rotation du personnage quand on appuie sur les touches "Horizontal"
        transform.Rotate(Vector3.up * turnSpeed * Time.fixedDeltaTime * Input.GetAxis("Horizontal"));

        //Avancée du personnage selon les touches "Vertical"
        transform.Translate(Vector3.forward * currentSpeed * Time.fixedDeltaTime * Input.GetAxis("Vertical"));

        //Bruit de déplacement
        if (Input.GetButton("Vertical") || Input.GetButton("Horizontal"))
        {
            if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                playerAudio.pitch = 1.2f;
            }
            else
            {
                playerAudio.pitch = 1f;
            }

            if (!playerAudio.isPlaying)
            {
                playerAudio.Play();
            }
        }
        else
        {
            playerAudio.Stop();
        }
    }

    public void GameOver()
    {
        affichageGameOver.SetActive(true);
        StartCoroutine(LoadMenu());
    }

    IEnumerator LoadMenu()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Menu");
    }
}
