﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAllPrefs : MonoBehaviour
{
    // Awake est appelé quand l'instance de script est chargée
    private void Awake()
    {
        PlayerPrefs.DeleteAll();
    }
}
